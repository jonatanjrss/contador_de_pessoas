from PIL import Image


def fit_cover(img, parent_size):
    parent_size = parent_size

    output_size_width = parent_size[0]
    output_size_height = parent_size[1]

    imagem = Image.open(img)

    input_size_width = imagem.size[0]
    input_size_height = imagem.size[1] 

    if output_size_width/output_size_height > input_size_width/input_size_height:
        output_size = (input_size_width, (input_size_width*output_size_height)/output_size_width,)
    else:
        output_size = (
            int(input_size_height*output_size_width/output_size_height),
            int(input_size_height) ,)
    
    w, h = output_size
    w_inicial = (input_size_width-w)/2
    w_final = input_size_width - (w_inicial)
    nova_imagem = imagem.crop((w_inicial, 0, w_final, h)) #width_inicial, height_inicial, width_final, height_final
    
    nome = img[:-4]
    extensao = img[-4:]
    
    nova_imagem.save(nome +'-cover' + extensao)