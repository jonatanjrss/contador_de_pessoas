import os

from kivy.app import App
from kivy.uix.anchorlayout import AnchorLayout
from kivy.core.window import Window
from kivy.properties import StringProperty
from kivymd.theming import ThemeManager
from android.runnable import run_on_ui_thread
from jnius import autoclass

from utils import fit_cover


WindowManager = autoclass('android.view.WindowManager$LayoutParams')
activity = autoclass('org.kivy.android.PythonActivity').mActivity


class MainWindow(AnchorLayout):
    fullscreen = False
    people = 0
    msg = "Pode entrar"
    img = StringProperty('img/restaurant-cover.jpg')

    def __init__(self, **kwargs):
        if not os.path.exists(self.img):
            fit_cover('img/restaurant.jpg', Window.size)
        super(MainWindow, self).__init__(**kwargs)
        self.change_statusbar()

    def change_statusbar(self):
        self.set_fullscreen()
        while self.fullscreen is not True:
            pass

    @run_on_ui_thread
    def set_fullscreen(self):
        window = activity.getWindow() 
        window.setFlags(WindowManager.FLAG_LAYOUT_NO_LIMITS,
                        WindowManager.FLAG_LAYOUT_NO_LIMITS)

        self.fullscreen = True

    def change_people(self, increment, instance=None):
        self.people += increment
        if self.people < 0:
            self.msg = "Mundo invertido"
        elif self.people > 10:
            self.msg = "Lotado"
        elif self.people <= 10:
            self.msg = "Pode entrar"
        self.update_screen()

    def update_screen(self):
        self.ids.people.text = f"Pessoas: {self.people}"
        self.ids.msg.text = self.msg


class PeopleAccountant(App):
    title = "Contador de Pessoas"
    theme_cls = ThemeManager()


if __name__ == '__main__':
    PeopleAccountant().run()